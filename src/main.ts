import { createSSRApp } from "vue"
import { createPinia } from "pinia"
import App from "./App.vue"
import MyComponent from "@/utils/install.ts"
export function createApp() {
  const app = createSSRApp(App)
  app.use(MyComponent)
  app.use(createPinia())
  return {
    app
  }
}
