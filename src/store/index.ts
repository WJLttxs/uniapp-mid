import { defineStore } from "pinia"
export const useMainStore = defineStore("prefectStore", {
  // a function that returns a fresh state
  state: () => ({
    barIndex: 0,
    name: "Eduardo"
  }),

  // optional actions
  actions: {
    reset(idx: number) {
      // `this` is the store instance
      this.barIndex = idx
    }
  }
})
