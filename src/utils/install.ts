import WTbar from "@/components/WTbar/index"
interface Type {
  install(T: any): void
}
const componetsEasyDom: Type = {
  install(Vue) {
    Vue.component("WTbar", WTbar)
  }
}
export default componetsEasyDom
