interface LoginRes {
  code: string
}

export const isLoginHandleFn = (): void => {
  !uni.getStorageSync("INFO") &&
    uni.navigateTo({
      url: "/pages/login/index"
    })
}

export const loginCode = async () => {
  try {
    const { code } = ((await uni.login({
      provider: "weixin"
    })) as unknown) as LoginRes
    uni.setStorageSync("INFO", code)
    uni.showToast({
      title: "您已经成功登录!",
      duration: 1000
    })
    uni.showLoading({
      title: "加载中"
    })
    setTimeout(() => {
      uni.navigateTo({
        url: "/pages/index/index"
      })
    }, 500)
  } catch (error) {
    uni.showModal({
      title: "错误",
      content: "请重新授权获取登录信息",
      success: function (res) {
        if (res.confirm) {
          loginCode()
        } else if (res.cancel) {
          loginCode()
        }
      }
    })
  }
}
